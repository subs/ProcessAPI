import re
import fnmatch
import os
import json

#####
# Primary useful function: gets the process info as JSON data.
# Default is to get the info for all processes.
# Return empty list if the process is not on the system.
#####
def getInfoAsJSON(pid=None):
    if pid==None:
        allpsinfo=dict()
        for pid in fnmatch.filter(os.listdir('/proc'),'[0-9]*'):
            allpsinfo[pid]=getOnePid(pid)
        return json.dumps(allpsinfo, indent=4)
    else:
        try:
            # Exact match so at most one element:
            pid = fnmatch.filter(os.listdir('/proc'),pid)[0]
            return json.dumps(getOnePid(pid), indent=4)
        except IndexError:
            return json.dumps([])

def getOnePid(pid):
    psinfo=dict()
    # Extract the data into given key of psinfo.
    # Functional style is overkill again, but could be extended easily.
    readFromProc(extractCmdline,'/proc/%s/cmdline' % pid, psinfo, 'cmdline')
    readFromProc(extractPpid,   '/proc/%s/stat' % pid,    psinfo, 'ppid')
    readFromProc(extractName,   '/proc/%s/comm' % pid,    psinfo, 'name')
    readFromProc(extractEnviron,'/proc/%s/environ' % pid, psinfo, 'environ')
    return psinfo

####
# Apply function below to raw data in /proc. If permission denied, return [].
####
def readFromProc(func,filename,data,key):
    try:
        f=open(filename, 'r')
        data[key]=func(f.read())
        f.close()
    except IOError:
        data[key]=[]

#####
# Extract data out of /proc. Make corrections:
# -- dump extra character that is always at end of comm file
# -- dump extra blank environment string that also in proc.
#####
def extractCmdline(cmddata):
    cmdline=' '.join(re.split('\x00',cmddata))
    if len(cmdline)==0:
        return False
    else:
        return cmdline[0:len(cmdline)-1]  # Extraneous space from split() function.

def extractPpid(statdata):
    return int(re.split(' ',statdata)[3])

def extractName(name):
    return name[0:len(name)-1]

def extractEnviron(env):
    environment=re.split('\x00',env)
    if len(environment) > 0:
        environment.pop()
    return environment
